angular.module('starter.controller', []).controller('despachoController', 
	['$scope', '$rootScope', 'despachoService', 
	function($scope, $rootScope, despachoService){

	$scope.getDespachos=function(){
		despachoService.getDespachos().success(function(data){
			$scope.despachos=data;
		});	
	};

    $scope.getDespachos();
}]);