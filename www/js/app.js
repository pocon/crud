// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
angular.module('starter', ['ionic', 'starter.controller', 'starter.service', 'starter.constant'])

.run(function($ionicPlatform, $http) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if(window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if(window.StatusBar) {
      StatusBar.styleDefault();
    }
  });
  var defaultHTTPHeaders={
    'Content-Type':'application/json',
    'Accept':'application/json',
    'X-Ionic-Application-Id': 'AIzaSyCKPZQ20sECdtnrMAnNqzVo5H8f4sHMs_I'
  };

  $http.defaults.headers.post=defaultHTTPHeaders;
})
.config(function($stateProvider, $urlRouterProvider){
  $stateProvider.state('app', {
    url:'/app',
    abstract:true,
    templateUrl:'templates/main.html'
  })
  .state('app.list', {          
    url: '/list',
    views: {
      'content': {
        templateUrl: 'templates/list.html',
        controller: 'despachoController'
      }
    }
  })
  .state('app.single', {          
    url: '/single',
    views: {
      'content': {
        templateUrl: 'templates/single.html'
      }
    }
  })
  $urlRouterProvider.otherwise('/app/list');
});
